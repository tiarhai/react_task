import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import { hashHistory } from 'react-router'
import { reducer as formReducer } from 'redux-form'

import User from './reducer_user';

let initialState = {};

const Store = createStore(
    combineReducers({
        routing: routerReducer,
        form: formReducer,
        user: User
    }),
    initialState,
    compose(
        applyMiddleware(routerMiddleware(hashHistory))
    )
);

export default Store;
