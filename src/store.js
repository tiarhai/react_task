import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import { hashHistory } from 'react-router'

let initialState = {};

const Store = createStore(
    combineReducers({
        routing: routerReducer
    }),
    initialState,
    compose(
        applyMiddleware(routerMiddleware(hashHistory))
    )
);


export default Store;

