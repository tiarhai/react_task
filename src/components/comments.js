// LOOP COMMENTS AND COMMENT BUTTONS, CREATING HTML
import React, { Component } from 'react';

import CommentOptions from './comment-options';

export default class Comments extends Component {
  renderList() {
    return this.props.comments.map(comment => {
      return (
        <div className="comment-container pb-1" key={ comment.id }>
          <div className="float-l">
            <img
              className="comment-img"
              src="/src/img/avatar.png"
            />
          </div>
          <div className="comment-content-block">
            <div className="relative b-02">
              <span className="fs-12 text-muted">{ comment.name }</span>
            </div>
            <div className="relative b-02 lh-1">
              <span className="fs-15">{ comment.content }</span>
            </div>
          </div>
          <CommentOptions />
        </div>
      )
    });
  }

  render() {
    return (
      <div>
        { this.renderList() }
      </div>
    );
  }
}
