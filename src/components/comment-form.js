// FORM (CREATE COMMENT) SECTION
// USES redux-form FOR STATE AND VALIDATION HANDLING
import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm } from 'redux-form';

class CommentForm extends Component {
  // handle submission
  onSubmit(values) {
    const { reset } = this.props;

    // simulating post to backend api
    setTimeout(() => {
      this.props.addComment(values)
      reset();
    }, 1500);
  }

  // BUILD SINGLE INPUT
  renderCommentInput(field) {
    const { meta: { touched, error } } = field;
    const className = `${touched && error ? " error-msg " : ""}`;

    return (
      <div>
        <div>
          <input
            type="text"
            className={"comment-form-input " + className }
            placeholder="Add a comment..."
            value={ field.initValue }
            { ...field.input }
          />
        </div>
        <div>
          <span className={ className }>{ touched ? error : "" }</span>
        </div>
      </div>
    )
  }

  // RENDER FULL ELEMENT
  render() {
    const { handleSubmit } = this.props;

    return (
      <div>
        <div className="float-l">
          <img
            className="comment-img"
            src="/src/img/avatar.png"
          />
        </div>
        <div className="comment-content-block relative b-02">
          <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) }>
            <Field
              name="new_comment"
              isVisible=""
              component={ this.renderCommentInput }
            />
            <div>
              <input type="submit" className="hide" />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

// VALIDATE INPUT
function validate(values) {
  const errors = {};

  if(!values.new_comment) {
    errors.new_comment = "Please enter a comment!";
  }
  else if(values.new_comment.length > 25) {
    errors.new_comment = "Comments cannot have more than 25 characters";
  }

  // If returns an empty object, no error
  return errors;
}

// wire up reduxForm to our form component
export default reduxForm({
  validate,
  form: 'PostsNewComment'
})(CommentForm);
