// LIKE | REPLY | VIEW REPLIES
// COMMENT BUTTON OPTIONS HTML

import React, { Component } from 'react';

export default class Comments extends Component {
  render() {
    return (
      <div>
        <div className="comment-options pr-05">
          <a href="#" className="fs-12 underline text-muted">Like</a>
        </div>
        <div className="comment-options pr-05">
          <a href="#" className="fs-12 underline text-muted">Reply</a>
        </div>
        <div className="comment-options pr-05">
          <a href="#" className="fs-12 underline text-muted">View Replies</a>
        </div>
      </div>
    );
  }
}
