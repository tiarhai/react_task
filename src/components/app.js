import React, { Component } from 'react'
import { connect } from 'react-redux'

import Comments from './comments';
import CommentForm from './comment-form';

/**
 * Main Wrapper
 */
class App extends Component {
  constructor(props) {
    super(props);

    // "pulled" comments from backend
    this.state = {
      comments: [{
        id: 0,
        name: "Janet Ryan",
        content: "I really like the way you have broken down the material. How does one model the transition state?" },
      { id: 1,
        name: "Alice Kelly",
        content: "Have you ever tried to carry out an experiment to determine if the flow is turbulent or not?" }
      ]
    };
  }

  // "push" comment to backend
  addComment(comment) {
    this.setState({
      comments: this.state.comments.concat({ id: this.state.comments.length, name: this.props.user.name, content: comment.new_comment })
    });
  }

  render() {
    return (
      <div className="container mt-2">
        <h6>Comments</h6>
        <CommentForm addComment={ comment => this.addComment(comment) }/>
        <hr className="mt-05 mb-08" />
        <Comments comments={ this.state.comments }/>
      </div>
    )
  }
}

// redux reducer for logged user
function mapStateToProps(state) {
  return {
    user: state.user
  };
}

export default connect(mapStateToProps)(App);
