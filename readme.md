For this task you would need to simulate API. Just write function in separate js file and use timeout to simulate async request.


**Acceptance criteria**:

* Form for creating comments
* Simulate async request to the api with timeout function(give 1.5 second timeout).
* If comment less or equal to 25 symbols then submit it.
* If comment more then 25 symbols then show error
* Validation should be done in simulated function(it should simulate backend validation error)
* Handle error on frontend and show error on UI.
* List of comments
* Show some indicator to user while request is pending
* Use Design.png for reference
* When finished create pull request or fork


**What matters**:

* Clean code
* Clean html/scss
* UI/UX quality
* Clean structure of files
* Code styling/Naming
* Reusability of component


**No need to implement**:

* Reply, like, view replies


In case if you will finish everything, and you will have ideas how to improve your implementation but won't have enough time, you can write in comments what you would have done.